package org.simprints.simprintsstub;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

/**
 * This is just a basic stub of what might be a simprints activity. It returns
 * a String that might be a fingerprint scan.
 *
 * @author sudar.sam@gmail.com
 */
public class AcquirePrintStub extends Activity {

  /** The result key returned from the activity as part of the result. */
  private static final String INTENT_KEY_RESULT = "value";

  /**
   * Called when the activity is first created.
   */
  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.main);
    // Now we'll set up the buttons for sending data back to the calling
    // activity.
    Button catButton = (Button) this.findViewById(R.id.button_return_cat);
    Button unicodeButton =
        (Button) this.findViewById(R.id.button_return_unicode);
    this.setClickListenerToReturnString(catButton, getString(R.string.cat));
    this.setClickListenerToReturnString(
        unicodeButton, getString(R.string.unicode_string));
  }

  /**
   * Add a click listener to the button that will call @#completeAndReturnString
   * when pressed, passing the result to the final activity.
   * @param button
   * @param result
   */
  private void setClickListenerToReturnString(Button button,
                                              final String result) {
    button.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        completeAndReturnString(result);
      }
    });
  }

  /**
   * Returns the result in an intent. Completes the activity.
   * @param result
   */
  private void completeAndReturnString(String result) {
    Intent intent = new Intent();
    intent.putExtra(INTENT_KEY_RESULT, result);
    this.setResult(Activity.RESULT_OK, intent);
    this.finish();
  }
}
