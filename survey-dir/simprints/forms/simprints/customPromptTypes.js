define(['promptTypes', 'jquery', 'underscore', 'prompts'],
function(promptTypes,   $,        _) {
    // Based on breathcounter/customPromptTypes.js.
    return {
        "simprints" : promptTypes.launch_intent.extend({
            type: "simprints",
            datatype: "simprints", // set in the form xlsx sheet
            buttonLabel: {
                'default': 'Launch Simprints'
            },
            intentString: 'org.simprints.simprintsstub.AcquirePrintStub',
            extractDataValue: function(jsonObject) {
                // For this we only need to return a single value, becase our
                // bundle is only going to be returning a string.
                return jsonObject.result.value;
            }
        })
    };
});
